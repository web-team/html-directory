$(document).ready(function () {

	var $window = $(window);
	if ($window.width() < 1200) {
		$('.nav.nav-tabs').removeClass('nav-tabs').addClass('nav-pills');
	}else{
		$('.nav.nav-pills').addClass('nav-tabs').removeClass('nav-pills');
	}
	$(window).resize(function() {
		if ($window.width() < 1200) {
			$('.nav.nav-tabs').removeClass('nav-tabs').addClass('nav-pills');
		}
		else{
			$('.nav.nav-pills').addClass('nav-tabs').removeClass('nav-pills');
		}
	});

	$("input#phone-search").focus();

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			$("input#phone-search").focus();
		})
});


$(function () {

  $('#map-tab #cern-searchform').submit(function () {
  $("#map-search").val("['"+$("#map-search").val()+"']");

    if (this.href !== undefined) {
      trackOutboundLink(this, 'Search form', $('option:selected', this).data('description'));
    }
  });

	$('#service-tab #cern-searchform').submit(function (e) {
		e.preventDefault();
		var url = 'https://cern.service-now.com/service-portal/?id=search&spa=1&q=' + $("#service-search").val();
    window.open(url, "_self");
	});


// clear fields on focus
	$(':input').not('select').bind('focus', function () { clearfield(this); }).bind('blur', function () { unclearfield(this); });


// page customization
		$('.sort-up, .sort-down').click(function (e) {

			var t = $(this),
					sect = $(this).closest('.sect-horiz'),
					anchor = $('#dir-nav [href="#' + sect.attr('id') + '"]').closest('li');

			sect.addClass('hide');
			anchor.addClass('hide');

			setTimeout(function () {

				if (t.hasClass('sort-up') && sect.prevAll('.sect-horiz:first').length) {

					sect.insertBefore(sect.prevAll('.sect-horiz:first'));
					anchor.insertBefore(anchor.prevAll('li:first'));

				}
				if (t.hasClass('sort-down') && sect.nextAll('.sect-horiz:first').length) {

					sect.insertAfter(sect.nextAll('.sect-horiz:first'));
					anchor.insertAfter(anchor.nextAll('li:first'));

				}

				setTimeout(function () {

					sect.removeClass('hide');
					anchor.removeClass('hide');

				}, 30);

			}, 300);

			cern.prevent(e);



		});

		$('.sort-remove').click(function (e) {

			var t = $(this),
					sect = $(this).closest('.sect-horiz'),
					anchor = $('#dir-nav [href="#' + sect.attr('id') + '"]').closest('li');

			sect.addClass('hide');
			anchor.addClass('hide');

			setTimeout(function () {

				sect.remove();
				anchor.remove();

			}, 300);

			cern.prevent(e);

		});

		$('#sort-page').click(function (e) {

			if ($('#directory').hasClass('sort')) {
				$('#directory').removeClass('sort');
				$('#sort-page').html('Customize this page');
			}
			else {
				$('#directory').addClass('sort');
				$('#sort-page').html('Save customization');
			}

			cern.prevent(e);

		});

		fixscroll($('#dir-nav'));

		// add tel links

		if (screen.width < 749) {
			$('#help li a').each(function () {

				if ($(this).attr('href').match('http://')) $(this).attr({ href: 'tel:+412276' + $(this).find('span').html() });

			});
		}

	});

	fixscroll = function (ele, pad) {

		var pad = pad || 20,
				top = ele.parent().offset().top,
				ws = $(window).scrollTop();

		$(window).unbind('scroll').scroll(function () { fixscroll(ele, pad); });

		if (ws > (top - pad)) ele.css({ top: pad + 'px' });
		else ele.css({ top: (top - ws) + 'px' });

	};

	clearfield = function (t) {
		//if ($(t).val() == $(t).attr('title'))
		$(t).val('');
	};

	unclearfield = function (t) {
		if ($(t).val() == $(t).attr('title') || $(t).val() == '') $(t).val($(t).attr('title'));
	};

	trackOutboundLink = function (link, category, action) {
 		//Track outbound destinations so we can build a 'most popular' block
		try {
		_gaq.push(['_trackEvent', category , action]);
		} catch(err){}

		setTimeout(function() {
      document.location.href = link.href;
		}, 100);
};
